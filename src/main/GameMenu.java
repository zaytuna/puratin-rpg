package main;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

import lowlevel.MarbleFilter;

public class GameMenu extends JPanel {
	private static final long serialVersionUID = -9213374513239907159L;
	public static BufferedImage menu;

	JButton b = new JButton("Continue");
	GameWindow gw;

	public GameMenu(GameWindow w) {
		try {
			MarbleFilter f = new MarbleFilter();
			f.setAmount(20);
			menu = f.filter(ImageIO.read(new File("Resources/MENU.jpg")), null);
		} catch (Exception e) {
		}

		setBackground(Color.WHITE);

		gw = w;
		setLayout(null);
		b.setBounds(GameWindow.SCREEN.width * 5 / 8, GameWindow.SCREEN.height * 5 / 6,
				GameWindow.SCREEN.width * 3 / 8 - 20, GameWindow.SCREEN.height / 6 - 10);

		b.setForeground(Color.ORANGE);
		b.setBackground(Color.BLUE);
		b.setFocusPainted(false);
		b.setFont(new Font("SANS_SERIF", Font.BOLD + Font.ITALIC, 40));
		add(b);
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				gw.toDisplay();
			}
		});
	}

	public void paintComponent(Graphics g3) {
		super.paintComponent(g3);

		Graphics2D g = (Graphics2D) g3;

		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f));
		g.drawImage(menu, 0, 0, GameWindow.SCREEN.width, GameWindow.SCREEN.height, this);
		g.setColor(Color.BLACK);
		g.fillRoundRect(30, 30, GameWindow.SCREEN.width - 60, GameWindow.SCREEN.height * 5 / 6 - 50, 100, 100);
		g.setComposite(AlphaComposite.SrcOver);

		g.setFont(new Font("SERIF", Font.PLAIN, 25));

		g.setColor(Color.WHITE);
		g.drawString("I know I'm the chosen one. The Elite. The Elect. I'm an ambasador from god.", 60, 120);
		g.drawString("I've known ever since my parents first told me I was special.", 60, 160);
		g.drawString("And here's the perfect opportunity to prove it: ", 60, 200);
		g.drawString("Since people are fightened to death of witches now-a-days, I "
				+ "reckon that I might as well get some attention that way.", 60, 240);
		g.drawString("If God had thought these people were innocent, he would not have given me this brilliant idea.", 60, 280);
		g.drawString("By reciting scripture publicly, I can boost my minion count.", 60, 360);
		g.drawString("Eventually, I'll have enough support to chalenge the minister.", 60, 320);
		g.drawString("But I have to remember to attend church every minute...", 60, 400);
		
	}
}
