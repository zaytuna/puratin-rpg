package main;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

import lowlevel.Methods;

public class GameDisplay extends JPanel implements KeyListener, ActionListener, Runnable {
	private static final long serialVersionUID = -2073819849211269275L;
	// percentage points wide/tall a grass tile is:
	public static final double GRASSX = 5, GRASSY = 5;
	public static int ROAD_MODE = 0;
	GameWindow owner;

	boolean accusing = false;

	// pixels/percentage point
	// public static final double SCALE = 100;

	public static BufferedImage grassImage, rockImage, person1, person2, person3, person4, mePerson, church, hut,
			tree1, tree2, house, burn, priest, script;

	Player player = new Player();
	Set<Integer> keysDown = new HashSet<Integer>();
	ArrayList<MapObject> objects = new ArrayList<MapObject>();

	MapObject engaged = null;

	long last = System.currentTimeMillis();
	double health = 20, opHealth = 20;

	double time = 0, angle = 0, angleTo = 0;

	JButton accuse = new JButton("You're a WITCH!");

	public GameDisplay(GameWindow gw) {
		owner = gw;
		setBackground(Color.BLACK);

		setDoubleBuffered(true);
		setLayout(null);

		accuse.setBounds(GameWindow.SCREEN.width - 310, GameWindow.SCREEN.height - 100, 300, 95);
		accuse.setFont(new Font("SERIF", Font.BOLD, 30));
		accuse.setBackground(Color.BLUE);
		add(accuse);
		accuse.setVisible(false);

		try {
			grassImage = ImageIO.read(new File("Resources/res.jpg"));
			rockImage = ImageIO.read(new File("Resources/ROCK_TILEABLE.jpg"));
			person1 = ImageIO.read(new File("Resources/PERSON1.png"));
			person2 = ImageIO.read(new File("Resources/PERSON2.png"));
			person3 = ImageIO.read(new File("Resources/PERSON3.png"));
			person4 = ImageIO.read(new File("Resources/PERSON4.png"));
			mePerson = ImageIO.read(new File("Resources/ME.png"));
			church = ImageIO.read(new File("Resources/CHURCH.png"));
			hut = ImageIO.read(new File("Resources/HUT.png"));
			tree1 = ImageIO.read(new File("Resources/TREE.png"));
			tree2 = ImageIO.read(new File("Resources/FALL_TREE.png"));
			house = ImageIO.read(new File("Resources/HOUSE.png"));
			burn = ImageIO.read(new File("Resources/BURNT_HUT.png"));
			priest = ImageIO.read(new File("Resources/HIGH_PRIEST.png"));
			script = ImageIO.read(new File("Resources/SCRIPTURE.png"));
		} catch (Exception e) {

		}

		addKeyListener(this);
		accuse.requestFocus();
		accuse.addActionListener(this);
		accuse.addKeyListener(this);
		new Thread(this).start();

		load();
		if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() != null)
			KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner().addKeyListener(this);
	}

	public void setVisible(boolean b) {
		super.setVisible(b);
		last = System.currentTimeMillis();
	}

	public void paintComponent(Graphics g2) {
		super.paintComponent(g2);
		Graphics2D g = (Graphics2D) g2;
		// draw grass textures

		for (double i = Math.max(roundDown(player.px - player.zoomX, GRASSX), 0); i < Math.min(
				roundUp(player.px + player.zoomX, GRASSX), 100); i += GRASSX) {
			for (double j = Math.max(roundDown(player.py - player.zoomY, GRASSY), 0); j < Math.min(
					roundUp(player.py + player.zoomY, GRASSY), 100); j += GRASSY) {
				g.drawImage(grassImage, GameWindow.SCREEN.width / 2
						+ (int) (GameWindow.SCREEN.width * (i - player.px) / (player.zoomX)), GameWindow.SCREEN.height
						/ 2 + (int) (GameWindow.SCREEN.height * (j - player.py) / (player.zoomY)), (int) (GRASSX
						* GameWindow.SCREEN.width / (player.zoomX)) + 1,
						(int) (GRASSY * GameWindow.SCREEN.height / (player.zoomY)) + 1, this);
			}
		}

		for (MapObject o : objects) {
			// System.out.println("Drawing " + o.name);
			if (o.type == MapObject.ROAD) {
				if (ROAD_MODE == 1) {
					double q = -(o.posX - o.sizeX) / (o.posY - o.sizeY);
					double dx = Math.sqrt(7 / (q * q + 1));
					double dy = Math.sqrt(7 * q * q / (q * q + 1));

					Polygon p = new Polygon(
							new int[] {
									GameWindow.SCREEN.width
											/ 2
											+ (int) (GameWindow.SCREEN.width * (o.posX - player.px + dx) / player.zoomX),
									GameWindow.SCREEN.width
											/ 2
											+ (int) (GameWindow.SCREEN.width * (o.posX - player.px - dx) / player.zoomX),
									GameWindow.SCREEN.width
											/ 2
											+ (int) (GameWindow.SCREEN.width * (o.sizeX - player.px - dx) / player.zoomX),
									GameWindow.SCREEN.width
											/ 2
											+ (int) (GameWindow.SCREEN.width * (o.sizeX - player.px + dx) / player.zoomX) },
							new int[] {
									GameWindow.SCREEN.height
											/ 2
											+ (int) (GameWindow.SCREEN.height * (o.posY - player.py - dy) / player.zoomY),
									GameWindow.SCREEN.height
											/ 2
											+ (int) (GameWindow.SCREEN.height * (o.posY - player.py + dy) / player.zoomY),
									GameWindow.SCREEN.height
											/ 2
											+ (int) (GameWindow.SCREEN.height * (o.sizeY - player.py + dy) / player.zoomY),
									GameWindow.SCREEN.height
											/ 2
											+ (int) (GameWindow.SCREEN.height * (o.sizeY - player.py - dy) / player.zoomY) },
							4);
					g.setColor(Color.RED);

					g.fillPolygon(p);
				} else if (ROAD_MODE == 0) {
					g.clipRect((int) (GameWindow.SCREEN.width / 2 + GameWindow.SCREEN.width * (o.posX - player.px)
							/ player.zoomX), GameWindow.SCREEN.height / 2
							+ (int) (GameWindow.SCREEN.height * (o.posY - player.py) / player.zoomY),
							GameWindow.SCREEN.width / 2
									+ (int) (GameWindow.SCREEN.width * (o.sizeX - player.px) / player.zoomX)
									- (int) (GameWindow.SCREEN.width * (o.posX - player.px) / player.zoomX),
							GameWindow.SCREEN.height / 2
									+ (int) (GameWindow.SCREEN.height * (o.sizeY - player.py) / player.zoomY)
									- (int) (GameWindow.SCREEN.height * (o.posY - player.py) / player.zoomY));

					for (int i = GameWindow.SCREEN.width / 2
							+ (int) (GameWindow.SCREEN.width * (o.posX - player.px) / player.zoomX); i < GameWindow.SCREEN.width
							/ 2 + (int) (GameWindow.SCREEN.width * (o.sizeX - player.px) / player.zoomX); i += (int) (2 * GameWindow.SCREEN.width / player.zoomX)) {
						for (int j = GameWindow.SCREEN.height / 2
								+ (int) (GameWindow.SCREEN.height * (o.posY - player.py) / player.zoomY); j < GameWindow.SCREEN.height
								/ 2 + (int) (GameWindow.SCREEN.height * (o.sizeY - player.py) / player.zoomY); j += (int) (2 * GameWindow.SCREEN.height / player.zoomY)) {
							g.drawImage(rockImage, i, j, (int) (2 * GameWindow.SCREEN.width / player.zoomX),
									(int) (2 * GameWindow.SCREEN.height / player.zoomY), this);
						}

					}

					g.setClip(null);
				}

			} else {
				g.drawImage(o.image, GameWindow.SCREEN.width / 2
						+ (int) (GameWindow.SCREEN.width * (o.posX - player.px) / player.zoomX),
						GameWindow.SCREEN.height / 2
								+ (int) (GameWindow.SCREEN.height * (o.posY - player.py) / player.zoomY),
						(int) (GameWindow.SCREEN.width * o.sizeX / player.zoomX), (int) (GameWindow.SCREEN.height
								* o.sizeY / player.zoomY), this);

				if (o == engaged && o.type != MapObject.CHURCH) {
					g.setColor(Color.RED);
					g.setStroke(new BasicStroke(30));
					g.drawOval(GameWindow.SCREEN.width / 2
							+ (int) (GameWindow.SCREEN.width * (o.posX - player.px) / player.zoomX),
							GameWindow.SCREEN.height / 2
									+ (int) (GameWindow.SCREEN.height * (o.posY - player.py) / player.zoomY),
							(int) (GameWindow.SCREEN.width * o.sizeX / player.zoomX), (int) (GameWindow.SCREEN.height
									* o.sizeY / player.zoomY));
				}
			}
		}
		g.translate(GameWindow.SCREEN.width / 2, GameWindow.SCREEN.height / 2);
		g.rotate(angle + Math.PI / 2);
		g.drawImage(mePerson, -(int) (GameWindow.SCREEN.width * 1.5 / player.zoomX),
				-(int) (GameWindow.SCREEN.height * 1.5 / player.zoomY), this);
		g.rotate(-angle - Math.PI / 2);
		g.translate(-GameWindow.SCREEN.width / 2, -GameWindow.SCREEN.height / 2);

		g.setFont(new Font("SANS_SERIF", Font.ITALIC, 35));

		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f));

		if (engaged != null) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, GameWindow.SCREEN.width, GameWindow.SCREEN.height);
			g.setColor(Color.WHITE);
			g.drawString(engaged.name, 20, 30);

			if (accusing) {
				Methods.drawCenteredText(g, g.getFont(),
						"A: Attack           D: Defend           X: ??? ... Run Away!", 0, 300,
						GameWindow.SCREEN.width, GameWindow.SCREEN.height);
				g.setColor(Color.GREEN);
				g.fillRect(100, GameWindow.SCREEN.height / 2 - 80,
						(int) ((GameWindow.SCREEN.width - 200) * health / 20), 75);
				g.setColor(Color.ORANGE);
				g.fillRect(100, GameWindow.SCREEN.height / 2, (int) ((GameWindow.SCREEN.width - 200) * opHealth / 20),
						75);

				g.setColor(Color.GREEN);
				Methods.drawCenteredText(g, g.getFont(), "The Marvelous Me", 100, GameWindow.SCREEN.height / 2 - 80,
						(int) ((GameWindow.SCREEN.width - 200)), 75);
				g.setColor(Color.ORANGE);
				Methods.drawCenteredText(g, g.getFont(), engaged.name, 100, GameWindow.SCREEN.height / 2,
						(int) ((GameWindow.SCREEN.width - 200)), 75);
			} else

				Methods.drawCenteredText(g, g.getFont(), engaged.info, 0, 0, GameWindow.SCREEN.width,
						GameWindow.SCREEN.height);
		}
		g.setColor(Color.WHITE);
		g.fillRoundRect(GameWindow.SCREEN.width - 210, 5, 205, 300, 30, 30);
		g.setComposite(AlphaComposite.SrcOver);

		g.setColor(Color.BLUE);
		g.setFont(new Font("SERIF", Font.PLAIN, 25));
		g.drawString("Followers: " + player.followers, GameWindow.SCREEN.width - 200, 50);
		g.drawString("Total: " + player.city, GameWindow.SCREEN.width - 200, 80);
		g.drawString("Church in " + Math.max((60 - (System.currentTimeMillis() - last) / 1000), 0),
				GameWindow.SCREEN.width - 200, 110);
		g.setStroke(new BasicStroke(1));

	}

	public void load() {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File("Resources/Map.SMITE")));
			String line = br.readLine();
			while (line != null) {
				if (!line.startsWith("//")) {
					String parts[] = line.split("\t");
					MapObject m = new MapObject();
					if (parts[0].equals("ROAD")) {
						m.type = MapObject.ROAD;
						m.name = parts[1];
						m.posX = Double.parseDouble(parts[2]);
						m.posY = Double.parseDouble(parts[3]);
						m.sizeX = Double.parseDouble(parts[4]);
						m.sizeY = Double.parseDouble(parts[5]);
						line = br.readLine();
						objects.add(m);
						continue;
					} else if (parts[0].equals("CHURCH")) {
						m.type = MapObject.CHURCH;
						m.image = church;
					} else if (parts[0].equals("HUT")) {
						m.type = MapObject.HUT;
						m.image = Math.random() > 0 ? house : hut;
					} else if (parts[0].equals("TREE")) {
						m.type = MapObject.TREE;
						m.image = Math.random() > .9 ? tree1 : tree2;
					} else if (parts[0].equals("PERSON")) {
						m.type = MapObject.PERSON;
						if (Math.random() < .25)
							m.image = person1;
						else if (Math.random() < .33)
							m.image = person2;
						else if (Math.random() < .5)
							m.image = person3;
						else
							m.image = person4;

					} else if (parts[0].equals("BOSS")) {
						m.type = MapObject.BOSS;
						m.image = priest;
					} else if (parts[0].equals("START LOCATION")) {
						player.px = Double.parseDouble(parts[2]);
						player.py = Double.parseDouble(parts[3]);
						line = br.readLine();
						continue;
					} else if (parts[0].equals("SCRIPTURE")) {
						m.type = MapObject.SCRIPTURE;
						m.image = script;
					} else
						throw new IllegalArgumentException("BAD FILE: " + parts[0]);

					// System.out.println("5%x8%".substring(3, 4));

					m.name = parts[1];
					m.posX = Double.parseDouble(parts[2]);
					m.posY = Double.parseDouble(parts[3]);
					m.info = parts[4];

					if (parts.length > 5) {
						m.sizeX = Double.parseDouble(parts[5]);
						m.sizeY = Double.parseDouble(parts[6]);
					}

					objects.add(m);
				}
				line = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static double roundDown(double d, double p) {
		return (Math.floor(d / p)) * p;
	}

	public static double roundUp(double d, double p) {
		return (Math.ceil(d / p)) * p;
	}

	public void run() {
		long lastLost = System.currentTimeMillis();
		if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() != null)
			KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner().addKeyListener(this);
		while (true) {
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}

			requestFocus();

			if (accusing && engaged != null) {
				health -= engaged.type == MapObject.BOSS ? .1 : Math.random() / 20;
				opHealth = Math.min(20, opHealth
						+ (engaged.type == MapObject.BOSS ? Math.random() / 10 : Math.random() / 20));

				if (health <= 0) {
					player.followers -= 50;
					accusing = false;
					engaged = null;
				} else if (opHealth <= 0) {
					if (engaged.type == MapObject.BOSS)
						owner.toMenu();
					player.followers += 20;
					objects.remove(engaged);
					accusing = false;
				}

			}

			double dx = 0, dy = 0;

			if (keysDown.contains(KeyEvent.VK_W))
				dy -= player.moveSpeed;
			if (keysDown.contains(KeyEvent.VK_S))
				dy += player.moveSpeed;
			if (keysDown.contains(KeyEvent.VK_A))
				dx -= player.moveSpeed;
			if (keysDown.contains(KeyEvent.VK_D))
				dx += player.moveSpeed;

			Point m = MouseInfo.getPointerInfo().getLocation();

			if (m.x == 0)
				dx -= player.moveSpeed;
			if (m.x == GameWindow.SCREEN.width - 1)
				dx += player.moveSpeed;
			if (m.y == 0)
				dy -= player.moveSpeed;
			if (m.y == GameWindow.SCREEN.height - 1)
				dy += player.moveSpeed;

			if (objects.contains(engaged) || keysDown.contains(KeyEvent.VK_SPACE)) {
				engaged = null;
				if (accuse.isVisible())
					accuse.setVisible(false);
			}

			for (int i = 0; i < objects.size(); i++) {
				MapObject o = objects.get(i);
				if (o.type != MapObject.ROAD && Math.abs(o.posX + o.sizeX / 2 - player.px) < 1.5 + o.sizeX / 2
						&& Math.abs(o.posY + o.sizeY / 2 - player.py) < 1.5 + o.sizeY / 2) {

					engaged = o;
					if (o.type != MapObject.CHURCH && keysDown.contains(KeyEvent.VK_SPACE)) {
						dx = -1
								* player.moveSpeed
								* (o.posX - player.px)
								/ ((o.posX - player.px) * (o.posX - player.px) + (o.posY - player.py)
										* (o.posY - player.py));
						dy = -1
								* player.moveSpeed
								* (o.posY - player.py)
								/ ((o.posX - player.px) * (o.posX - player.px) + (o.posY - player.py)
										* (o.posY - player.py));
					} else if (o.type != MapObject.CHURCH) {
						dx = 0;
						dy = 0;
					}
				}
			}

			if (Math.random() > .999)
				player.followers++;

			if (System.currentTimeMillis() - last > 60000) {
				if (System.currentTimeMillis() - lastLost > 500) {
					player.followers--;
					lastLost = System.currentTimeMillis();
				}
			}
			if (engaged != null) {
				if (!objects.contains(engaged)
						&& Math.abs(engaged.posX + engaged.sizeX / 2 - player.px) < 1.5 + engaged.sizeX / 2
						&& Math.abs(engaged.posY + engaged.sizeY / 2 - player.py) < 1.5 + engaged.sizeY / 2) {
					dx = 0;
					dy = 0;
				}
				if (engaged.type == MapObject.CHURCH) {
					last = System.currentTimeMillis();
				} else if (engaged.type == MapObject.SCRIPTURE && objects.contains(engaged)) {
					player.followers += 30;
					objects.remove(engaged);
				} else if ((engaged.type == MapObject.PERSON || (engaged.type == MapObject.BOSS && player.followers > 140))
						&& !accusing && objects.contains(engaged)) {
					accuse.setVisible(true);
				}
			}

			if (dy != 0 || dx != 0) {
				angleTo = Math.atan2(dy, dx);

				if (Math.abs(angle - angleTo) > Math.PI)
					if (angle < angleTo)
						angleTo -= Math.PI * 2;
					else
						angleTo += Math.PI * 2;

				angle %= 2 * Math.PI;

				player.px += dx;// Math.cos(angle) * player.moveSpeed;
				player.py += dy;// Math.sin(angle) * player.moveSpeed;
			}
			angle += (angleTo - angle) / 10;

			repaint();
		}
	}

	@Override
	public void keyPressed(KeyEvent evt) {
		if (accusing) {
			if (evt.getKeyCode() == KeyEvent.VK_A && !keysDown.contains(KeyEvent.VK_A))
				opHealth -= 1;

			if (evt.getKeyCode() == KeyEvent.VK_D && !keysDown.contains(KeyEvent.VK_D))
				health = Math.min(20, health + .1 + ((double) player.followers) / 100);

			if (evt.getKeyCode() == KeyEvent.VK_X) {
				player.followers -= 10;
				accusing = false;
				engaged = null;
			}
		}
		keysDown.add(evt.getKeyCode());

	}

	@Override
	public void keyReleased(KeyEvent evt) {
		keysDown.remove(evt.getKeyCode());
	}

	public void keyTyped(KeyEvent arg0) {
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		accusing = true;
		accuse.setVisible(false);
		accuse.addKeyListener(this);
		if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() != null)
			KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner().addKeyListener(this);
		health = 20;
		opHealth = 20;
	}
}
