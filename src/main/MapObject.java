package main;

import java.awt.image.BufferedImage;

public class MapObject {
	public static final int ROAD = 0, CHURCH = 1, HUT = 2, PERSON = 3, TREE = 4, BOSS = 5, SCRIPTURE = 6;
	
	BufferedImage image;
	int type;
	String name, info;
	double posX, posY, sizeX, sizeY;
}
